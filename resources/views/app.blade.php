<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
</head>
<body class="login-page">
	@yield('content')
</body>
</html>
