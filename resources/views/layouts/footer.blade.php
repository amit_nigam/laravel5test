    <footer class="main-footer">
            <div class="pull-right hidden-xs"> 
            </div>
            <strong>Copyright &copy; 2016 </a></strong> All rights reserved.
        </footer>
      	<script src="https://maps.google.com/maps/api/js?sensor=false"></script>  
      	
      	<script src="{{ URL::asset('js/jquery-1.11.3.min.js') }}"></script>
	    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	    <script src="{{ URL::asset('js/jquery-ui.js') }}"></script> 
      <script src="{{ URL::asset('js/product.js') }}"></script>
    </body>
</html>
