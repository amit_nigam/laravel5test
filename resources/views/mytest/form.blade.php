
<div class="col-md-6">
    
    <div class="form-group{{ $errors->first('FirstName', ' has-error') }}">
        <label class="col-lg-4 col-md-4 control-label"> Product nam <span class="error">*</span></label>
        <div class="col-lg-8 col-md-8"> 
            {!! Form::text('ProductName',null, ['class' => 'form-control form-cascade-control input-small','id'=>'ProductName'])  !!} 
            <span class="label label-danger">{{ $errors->first('ProductName', ':message') }}</span>
        </div>
    </div> 

    <div class="form-group{{ $errors->first('LastName', ' has-error') }}">
        <label class="col-lg-4 col-md-4 control-label">Quantity in stock</label>
        <div class="col-lg-8 col-md-8"> 
            {!! Form::text('Quantity_in_stock',null, ['class' => 'form-control form-cascade-control input-small','id'=>'Quantity_in_stock'])  !!}
            <span class="label label-danger">{{ $errors->first('Quantity_in_stock', ':message') }}</span>
        </div>
    </div>

    <div class="form-group{{ $errors->first('email', ' has-error') }}">
        <label class="col-lg-4 col-md-4 control-label">Price per item</label>
        <div class="col-lg-8 col-md-8"> 
            {!! Form::text('price',null, ['class' => 'form-control form-cascade-control input-small','id'=>'price'])  !!}
            <span class="label label-danger">{{ $errors->first('price', ':message') }}</span>
           
        </div>
    </div> 
     
     
    
    <div class="form-group">
        <label class="col-lg-4 col-md-4 control-label"></label>
        <div class="col-lg-8 col-md-8"> 
            {!! Form::submit('Submit', ['class'=>'btn btn-primary text-white']) !!}
        </div>
    </div> 

</div> 