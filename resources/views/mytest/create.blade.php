@extends('layouts.master') 
    @section('content') 

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper"> 
        <!-- Main content -->
          <section class="content">
            <!-- Small boxes (Stat box) -->
              <div class="row">
                  <div class="col-md-12">
                       <div class="panel panel-cascade">
                          <div class="panel-body ">
                              <div class="row">  
                                  {!! Form::model($productModel, ['route' => ['product.store'],'class'=>'form-horizontal','files' => true,'id'=>'product']) !!}
                                    @include('mytest.form')
                                  {!! Form::close() !!} 
                                      
                              </div>
                              <div class="row"> 
                                <div class="col-md-8">
                            @if(isset($json_data))
                                  <table border="1">
                                    <tr>
                                      <td>
                                        Product Name
                                      </td>
                                      <td>
                                        Quantity
                                      </td>
                                       <td>
                                        Price per unit
                                      </td>
                                       <td>
                                        Total Price
                                      </td>
                                      <td>
                                       Date
                                      </td> 
                                    </tr> 
                                    
                                    {{--*/ $totalPrice = 0 /*--}}
                                    {{--*/ $totalQty = 0 /*--}}
                                    @foreach($json_data as $key => $result)
                                    {{--*/ $totalPrice =  $totalPrice+$result['total_price'] /*--}}
                                     {{--*/ $totalQty = $totalQty+$result['Quantity_in_stock'] /*--}}
                                     <tr>
                                      <td>
                                      {{ $result['ProductName'] }}
                                      </td>
                                      <td>
                                        {{ $result['Quantity_in_stock'] }}
                                      </td>
                                       <td>
                                       {{ $result['price'] }}
                                      </td>
                                       <td>
                                       {{ $result['total_price'] }}
                                      </td>
                                       <td>
                                       {{ $result['date'] }}
                                      </td>
                                      <!-- <td>
                                        <a href="{{ route('product.create','id='.$json_data_id[$key])}}"> Edit </a>   </td>

                                    </tr>  -->
                                    @endforeach
                                    <tr>
                                      
                                      <td colspan="2">Total Quantity: {{$totalQty}}</td> 
                                      <td colspan="2">Total Price: {{ $totalPrice }}</td>
                                      <td></td>
                                    </tr>

                                  </table>
                                   @endif
                                </div> 
                              </div>

                          </div>
                    </div>
                </div>            
              </div>  
            <!-- Main row --> 
          </section><!-- /.content -->
      </div> 
@stop
