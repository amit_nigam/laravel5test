<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Product;
use Input;
use Html;
use Form; 
use File;
use Storage;
use Redirect;


class ProductController extends Controller
{
    
    public function __construct(Request $request) {
        
       //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
         return Redirect::to(route('product.create'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ProductRequest $request, Product $productModel)
    {
 
        $files = File::allFiles(storage_path().'/app');
        
        foreach ($files as $file)
        {
            $fname = rtrim(basename($file),'.txt'); 
            $data           = (string)$file;  
            $content        = File::get($data); 
            $json           = json_decode($content,true);
            $json_data[]    = json_decode($json,true); 
            $json_data_id[] = $fname;
        } 
        
        return view('mytest.create', compact('productModel','json_data','json_data_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request, Product $productModel)
    {
        $data = Input::all();
        $qty_in_stock = Input::get('Quantity_in_stock');
        $price = Input::get('price'); 
        $data['total_price'] = $qty_in_stock*$price; 
        $data['date']   =   date('Y-m-d');  
        $contents = json_encode($data);
        $rs = Storage::disk( 'local' )->put( time().'.txt' ,json_encode($contents));

        return Redirect::to(route('product.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Product $productModel)
    {
        dd($request);
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
