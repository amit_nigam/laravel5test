<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('product/addProduct', 'ProductController@addProduct');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController', 
]);

Route::post('/login', function(Request $request) {        
        $credentials = array('email' => Input::get('email'), 'password' => Input::get('password')); 
        if (Auth::attempt($credentials, true)) {
        	dd("Login Successfully");
        }
        return Redirect::to('auth/login')->with('flash_alert_notice', 'Wrong Email or Password. Try again !')->withInput();
});

Route::resource('home', 'HomeController@index', [
        'names' => [
            'index' => 'home',
        ]
            ]
    );

// Route::bind('product', function($value, $route) {
//         return App\Product::find($value);
//     });

    Route::resource('product', 'ProductController', [

        'names' => [
            'edit' => 'product.edit',
            'show' => 'product.show',
            'destroy' => 'product.destroy',
            'update' => 'product.update',
            'store' => 'product.store',
            'index' => 'product',
            'create' => 'product.create',
        ]
            ]
    );